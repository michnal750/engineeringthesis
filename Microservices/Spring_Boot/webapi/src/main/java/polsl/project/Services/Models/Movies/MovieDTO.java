package polsl.project.Services.Models.Movies;

import lombok.Data;
import polsl.project.Services.Models.Directors.DirectorDTO;

import java.util.Date;


@Data
public class MovieDTO {

    private Integer id;

    private String title;

    private Date releaseDate;

    private Float rating;

    private String plot;

    private DirectorDTO director;
}
