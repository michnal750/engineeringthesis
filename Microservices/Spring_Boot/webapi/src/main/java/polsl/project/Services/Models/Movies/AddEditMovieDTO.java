package polsl.project.Services.Models.Movies;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;


@Data
public class AddEditMovieDTO {

    private Integer id;

    @NotBlank(message = "Title cannot be null")
    private String title;

    private Date releaseDate;

    @Min(value = 1, message = "Rating must be equal or greater then 1")
    @Max(value = 10, message = "Rating must be equal or less then 10")
    private Float rating;

    private String plot;

    private Integer directorId;
}
