package polsl.project.Services.Services;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import polsl.project.DAL.Entities.Movie;
import polsl.project.DAL.Repositories.MovieRepository;
import polsl.project.Services.Interfaces.IMovieService;
import polsl.project.Services.Models.Movies.AddEditMovieDTO;
import polsl.project.Services.Mapper;
import polsl.project.Services.Models.Movies.MovieDTO;
import polsl.project.Services.Models.Movies.MovieListItemDTO;

import java.util.List;

@Service
public class MovieService implements IMovieService {

    @Autowired
    Mapper mapper;

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public MovieDTO addMovie(AddEditMovieDTO newMovieDTO) {
        Movie newMovie = mapper.map(newMovieDTO, Movie.class);
        newMovie  = movieRepository.save(newMovie);
        MovieDTO addedMovieDTO = mapper.map(newMovie, MovieDTO.class);
        return addedMovieDTO;
    }

    @Override
    public MovieDTO updateMovie(AddEditMovieDTO editedMovieDTO) {
        Movie movie = movieRepository.findById(editedMovieDTO.getId()).get();

        movie.setTitle(editedMovieDTO.getTitle());
        movie.setPlot(editedMovieDTO.getPlot());
        movie.setRating(editedMovieDTO.getRating());
        movie.setReleaseDate(editedMovieDTO.getReleaseDate());

        Movie updatedMovie  = movieRepository.save(movie);
        return mapper.map(updatedMovie, MovieDTO.class);
    }

    @Override
    public MovieDTO getMovieDetails(Integer Id) {
        Movie movie = movieRepository.findById(Id).get();
        MovieDTO movieDTO = mapper.map(movie, MovieDTO.class);
        return movieDTO;
    }

    @Override
    public void deleteMovie(Integer id) {
        Movie movie = movieRepository.findById(id).get();
        movieRepository.delete(movie);
        return;
    }

    @Override
    public List<MovieListItemDTO> getMovies() {
        List<MovieListItemDTO> listItemDTOS = mapper.map(movieRepository.findAll(), new TypeToken<List<MovieListItemDTO>>() {}.getType());
        return listItemDTOS;
    }
}
