package polsl.project.Services.Interfaces;

import polsl.project.Services.Models.Movies.MovieDTO;

import java.security.NoSuchAlgorithmException;

public interface IHashService {
    public abstract String hashText(String text) throws NoSuchAlgorithmException;
}
