package polsl.project.DAL.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import polsl.project.DAL.Entities.Director;

@Repository
public interface DirectorRepository extends CrudRepository<Director, Integer> {
}
