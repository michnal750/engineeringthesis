package polsl.project.DAL.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import polsl.project.DAL.Entities.Movie;

@Repository
public interface MovieRepository extends CrudRepository<Movie,Integer> {
}
