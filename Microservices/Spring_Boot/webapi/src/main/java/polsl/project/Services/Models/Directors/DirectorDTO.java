package polsl.project.Services.Models.Directors;

import lombok.Data;

@Data
public class DirectorDTO {

    public Integer id;

    public String firstName;

    public String lastName;
}
