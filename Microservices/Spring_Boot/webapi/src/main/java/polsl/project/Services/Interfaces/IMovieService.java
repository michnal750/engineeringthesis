package polsl.project.Services.Interfaces;

import polsl.project.Services.Models.Movies.AddEditMovieDTO;
import polsl.project.Services.Models.Movies.MovieDTO;
import polsl.project.Services.Models.Movies.MovieListItemDTO;

public interface IMovieService {
    public abstract MovieDTO addMovie(AddEditMovieDTO newMovie);
    public abstract MovieDTO updateMovie(AddEditMovieDTO editedMovie);
    public abstract MovieDTO getMovieDetails(Integer Id);
    public abstract void deleteMovie(Integer id);
    public abstract Iterable<MovieListItemDTO> getMovies();
}
