package polsl.project.DAL.Entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="Movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String title;

    private Date releaseDate;

    private Float rating;

    private String plot;

    @ManyToOne
    @JoinColumn(name="directorId", nullable=true)
    private Director director;
}
