package polsl.project.Services.Models.Movies;

import lombok.Data;

import java.util.Date;

@Data
public class MovieListItemDTO {

    private Integer id;

    public String title;

    public Date releaseDate;

    public Integer directorId;
}
