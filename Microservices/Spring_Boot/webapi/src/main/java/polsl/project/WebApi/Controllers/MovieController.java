package polsl.project.WebApi.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import polsl.project.Services.Models.Movies.AddEditMovieDTO;
import polsl.project.Services.Models.Movies.MovieDTO;
import polsl.project.Services.Models.Movies.MovieListItemDTO;
import polsl.project.Services.Services.MovieService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path="api/movies")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public @ResponseBody
    ResponseEntity<List<MovieListItemDTO>> getAllMovies() {
        List<MovieListItemDTO> moviesDTO = movieService.getMovies();
        return ResponseEntity.status(HttpStatus.OK).body(moviesDTO);
    }

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity<MovieDTO> getMovieDetails(@PathVariable Integer id){

        MovieDTO movieDTO = movieService.getMovieDetails(id);
        return ResponseEntity.status(HttpStatus.OK).body(movieDTO);
    }

    @PostMapping
    public @ResponseBody
    ResponseEntity<MovieDTO> addNewMovie(@Valid @RequestBody AddEditMovieDTO newMovieDTO)
    {
        MovieDTO addedMovieDTO = movieService.addMovie(newMovieDTO);
        return ResponseEntity.status(HttpStatus.OK).body(addedMovieDTO);
    }

    @PutMapping(path="/{id}")
    public @ResponseBody
    ResponseEntity updateMovie(@PathVariable Integer id, @Valid @RequestBody AddEditMovieDTO editedMovieDTO)
    {
        if(!id.equals(editedMovieDTO.getId()))
        {
            String message = "Invalid update Id: " + id + " != " + editedMovieDTO.getId();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        }
        MovieDTO updatedMovieDTO = movieService.updateMovie(editedMovieDTO);
        return ResponseEntity.status(HttpStatus.OK).body(updatedMovieDTO);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody
    ResponseEntity<String> deleteMovie(@PathVariable Integer id)
    {
        movieService.deleteMovie(id);
        String message = "Successfully deleted movie with id = " + id;
        return ResponseEntity.status(HttpStatus.OK).body(message);
    }

    @GetMapping(path="/test")
    public @ResponseBody ResponseEntity<String> test(){
        return ResponseEntity.status(HttpStatus.OK).body("Test");
    }
}
