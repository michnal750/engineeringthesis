package polsl.project.WebApi.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import polsl.project.Services.Services.HashService;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(path="api/hashes")
public class HashesController {

    @Autowired
    private HashService hashService;

    @GetMapping
    public @ResponseBody
    ResponseEntity<String> hashText(@RequestParam String text){
        try {
            if(text.isEmpty())
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("incorrect parameter");
            }
            String hashedText = hashService.hashText(text);
            return ResponseEntity.status(HttpStatus.OK).body(hashedText);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("incorrect algorithm");
        }
    }
}
