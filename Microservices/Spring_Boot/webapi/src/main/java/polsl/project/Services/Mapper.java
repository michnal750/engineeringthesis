package polsl.project.Services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

@Component
public class Mapper extends ModelMapper {

    public void Mapper(){
        //Add configuration if needed
        this.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
    }
}
