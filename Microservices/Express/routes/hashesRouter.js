const express = require("express");
const router = express.Router();

const movieController = require("../controllers/hashesController");

router.route('/').get(movieController.computeSha256Hash);

module.exports = router;