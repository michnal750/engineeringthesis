const express = require("express");
const router = express.Router();

const movieController = require("../controllers/moviesController");

const {
    validateMovie
  } = require('../validators/movieValidator');
  

router.route('/test').get(movieController.test);
router.route('/').get(movieController.getAll);
router.route('/:id').get(movieController.getDetails);
router.route('/').post(validateMovie, movieController.addNew);
router.route('/:id').put(validateMovie, movieController.update);
router.route('/:id').delete(movieController.delete);

module.exports = router;