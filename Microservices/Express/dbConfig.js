module.exports = {
    host: "db",
    user: "dbuser",
    password: "password",
    dbname: "moviesdb",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };