const {check, validationResult} = require('express-validator');

exports.validateMovie = [
  check('title')
    .not().isEmpty().withMessage('Movie title cannot not be empty')
    .isLength({max: 20}).withMessage('Movie title must have maximum 20 characters'),
  check('rating')
    .isFloat({ min: 1.0, max: 10.0 }).withMessage('Movie rating must be a number between 1.0 and 10.0'),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({errors: errors.array()});
    next();
  },
];