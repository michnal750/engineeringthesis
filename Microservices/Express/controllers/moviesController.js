const db = require("../entities");
const moviesRepository = db.movies;

exports.getAll = (req, res) => {
    moviesRepository.findAll({attributes: ['id', 'title', 'releaseDate', 'directorId']})
    .then(data => {
      res.status(200).send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving movies"
      });
    });
}

exports.getDetails = (req, res) => {
    moviesRepository.findByPk(req.params.id, {include: [{ all: true }]})
    .then(data => {
        res.status(200).send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving movies"
        });
      });
}

exports.addNew = (req, res) => {
    const newMovie =
    {
        title: req.body.title,
        releaseDate: req.body.release_date,
        rating: req.body.rating,
        plot: req.body.plot,
        directorId: req.body.director_id
    };

    moviesRepository.create(newMovie)
  .then(data => {
    moviesRepository.findByPk(data.id, {include: [{ all: true }]})
    .then(data => {
        res.status(200).send(data);
      });
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the movie"
    });
  });
}

exports.update = (req, res) => {
    const id = req.params.id;

    const updatedMovie =
    {
        id: req.body.id,
        title: req.body.title,
        releaseDate: req.body.release_date,
        rating: req.body.rating,
        plot: req.body.plot
    };

    if( id != req.body.id ){
        res.status(400).send({message: `Invalid update id: ${id} != ${req.body.id}`});
    }

    moviesRepository.update(updatedMovie, {
        where: { id: id }
      })
        .then(num => {
          if (num == 1) {
            moviesRepository.findByPk(id, {include: [{ all: true }]})
            .then(data => {
                res.status(200).send(data);
              });
          } else {
            res.status(400).send({
              message: `Cannot update movie with id = ${id}`
            });
          }
        })
        .catch(err => {
          res.status(500).send({
            message: `Error updating movie with id = ${id}`
          });
        });
}

exports.delete = (req, res) => {
    const id = req.params.id;
  
    moviesRepository.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.status(200).send({
            message: `Successfully deleted movie with id = ${id}`
          });
        } else {
          res.status(400).send({
            message: `Cannot delete movie with id = ${id}`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: `Could not delete movie with id = ${id}`
        });
      });
  };

  exports.test = (req, res) => {
          res.status(200).send({
             message: "test"
             });
  };
