var crypto = require('crypto');

exports.computeSha256Hash = (req, res) => {
    var text = req.query.text;
    if(!text)
    {
        res.status(400).send("Incorrect parameter");
    }
    text = crypto.createHash('sha256').update(text).digest('hex');
    res.status(200).send(text);
    
}
