module.exports = (sequelize, Sequelize) => {
    const Director = sequelize.define("directors", {
      firstName: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      lastName:{
        type: Sequelize.STRING(20),
        allowNull: false
      }
    },{
      timestamps: false,
      underscored: true,
      freezeTableName: true,
    });
    return Director;
  };