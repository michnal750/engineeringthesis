const dbConfig = require("../dbConfig");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.dbname, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.movies = require("./movie.model")(sequelize, Sequelize);
db.directors = require("./director.model")(sequelize, Sequelize);

db.movies.belongsTo(db.directors, {
  foreignKey: "directorId",
  as: "director",
});

module.exports = db;