module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define("movies", {
      title: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      releaseDate:{
        type:Sequelize.DATE(6)
      },
      rating: {
        type: Sequelize.FLOAT
      },
      plot: {
        type: Sequelize.STRING
      }
    },{
      timestamps: false,
      underscored: true,
      freezeTableName: true,
    });
    return Movie;
  };