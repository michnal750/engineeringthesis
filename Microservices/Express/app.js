'use strict';
const express = require("express");
const bodyParser = require("body-parser");

const moviesRouter = require("./routes/moviesRouter");
const hashesRouter = require("./routes/hashesRouter");

var app = express();
app.use(bodyParser.json());

app.use("/api/movies", moviesRouter);
app.use("/api/hashes", hashesRouter);

const db = require("./entities");
db.sequelize.sync({ alter: true, force: true }).then(() => {
    console.log("Drop and re-sync db.");
  });

const port = 3000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
});