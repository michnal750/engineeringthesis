﻿using System.Collections.Generic;

namespace MoviesWebApi.DAL.Entities
{
    public class Director
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
