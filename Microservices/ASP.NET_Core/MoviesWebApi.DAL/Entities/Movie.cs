﻿using System;

namespace MoviesWebApi.DAL.Entities
{
    public class Movie
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public float? Rating { get; set; }

        public string Plot { get; set; }

        public int? DirectorId { get; set; }
        public virtual Director Director { get; set; }
    }
}
