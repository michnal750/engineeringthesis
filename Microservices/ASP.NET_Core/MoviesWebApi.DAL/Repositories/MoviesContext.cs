﻿using Microsoft.EntityFrameworkCore;
using MoviesWebApi.DAL.Configurations;
using MoviesWebApi.DAL.Entities;

namespace MoviesWebApi.DAL.Repositories
{
    public class MoviesContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }

        public MoviesContext(DbContextOptions<MoviesContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration<Movie>(new MovieConfiguration());
            builder.ApplyConfiguration<Director>(new DirectorConfiguration());
            base.OnModelCreating(builder);
        }
    }
}
