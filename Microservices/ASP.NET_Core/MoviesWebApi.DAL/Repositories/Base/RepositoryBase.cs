﻿using Microsoft.EntityFrameworkCore;
using MoviesWebApi.DAL.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesWebApi.DAL.Repositories.Base
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected MoviesContext MoviesContext { get; set; }

        public RepositoryBase(MoviesContext moviesContext)
        {
            MoviesContext = moviesContext;
        }

        public void Add(T entity)
        {
            MoviesContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            MoviesContext.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            MoviesContext.Set<T>().Update(entity);
        }

        public async Task SaveChangesAsync()
        {
            await MoviesContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await MoviesContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await MoviesContext.Set<T>().FindAsync(id);
        }
    }
}
