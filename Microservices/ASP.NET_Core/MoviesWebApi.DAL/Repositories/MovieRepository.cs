﻿using Microsoft.EntityFrameworkCore;
using MoviesWebApi.DAL.Entities;
using MoviesWebApi.DAL.Interfaces.Repositories;
using MoviesWebApi.DAL.Repositories.Base;
using System.Threading.Tasks;

namespace MoviesWebApi.DAL.Repositories
{
    public class MovieRepository : RepositoryBase<Movie>, IMovieRepository
    {
        public MovieRepository(MoviesContext moviesContext) : base(moviesContext)
        {
        }

        public async Task<Movie> GetByIdWithDetailsAsync(int id)
        {
            return await MoviesContext
                .Set<Movie>()
                .Include(p => p.Director)
                .FirstOrDefaultAsync(e => e.Id == id);
        }
    }
}
