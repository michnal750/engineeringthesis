﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MoviesWebApi.DAL.Repositories
{
    public class MoviesContextFactory : IDesignTimeDbContextFactory<MoviesContext>
    {
        public MoviesContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MoviesContext>();
            optionsBuilder.UseMySql("server=db;database=moviesdb;uid=dbuser;password=password;");
            return new MoviesContext(optionsBuilder.Options);
        }
    }
}
