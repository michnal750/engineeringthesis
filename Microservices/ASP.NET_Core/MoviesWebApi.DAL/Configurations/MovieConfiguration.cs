﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoviesWebApi.DAL.Entities;

namespace MoviesWebApi.DAL.Configurations
{
    public class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(p => p.Id)
                .HasName("id");

            builder.Property(p => p.Id)
                .HasColumnName("id");

            builder.Property(p => p.Title)
                .HasColumnName("title")
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(p => p.ReleaseDate)
                .HasColumnName("release_date");

            builder.Property(p => p.Rating)
                .HasColumnName("rating")
                .HasColumnType("float");

            builder.Property(p => p.Plot)
                .HasColumnName("plot")
                .HasMaxLength(255);

            builder.Property(p => p.DirectorId)
                .HasColumnName("director_id");
        }
    }
}
