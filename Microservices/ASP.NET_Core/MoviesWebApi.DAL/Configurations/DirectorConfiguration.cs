﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoviesWebApi.DAL.Entities;

namespace MoviesWebApi.DAL.Configurations
{
    public class DirectorConfiguration : IEntityTypeConfiguration<Director>
    {
        public void Configure(EntityTypeBuilder<Director> builder)
        {
            builder.HasKey(p => p.Id)
                .HasName("id");

            builder.Property(p => p.Id)
            .HasColumnName("id");

            builder.Property(p => p.FirstName)
                .HasColumnName("first_name")
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(p => p.LastName)
                .HasColumnName("last_name")
                .HasMaxLength(20)
                .IsRequired();

            builder.HasMany(p => p.Movies)
                .WithOne(p => p.Director)
                .HasForeignKey(p => p.DirectorId);
        }
    }
}
