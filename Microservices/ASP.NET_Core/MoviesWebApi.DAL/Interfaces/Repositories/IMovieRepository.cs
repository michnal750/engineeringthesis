﻿using MoviesWebApi.DAL.Entities;
using MoviesWebApi.DAL.Interfaces.Repositories.Base;
using System.Threading.Tasks;

namespace MoviesWebApi.DAL.Interfaces.Repositories
{
    public interface IMovieRepository : IRepositoryBase<Movie>
    {
        Task<Movie> GetByIdWithDetailsAsync(int id);
    }
}
