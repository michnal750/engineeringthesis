﻿using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoviesWebApi.DAL.Interfaces.Repositories;
using MoviesWebApi.DAL.Repositories;
using MoviesWebApi.Services.Interfaces;
using MoviesWebApi.Services.Mapper;
using MoviesWebApi.Services.Models;
using MoviesWebApi.Services.Services;
using MoviesWebApi.Services.Validators;

namespace MoviesWebApi.WebApi
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContextPool<MoviesContext>(options =>
              options.UseMySql("server=db;database=moviesdb;uid=dbuser;password=password;"));
            return services;
        }

        public static IServiceCollection AddMappingServices(this IServiceCollection services)
        {
            services.AddSingleton<Profile, MovieProfile>();
            services.AddSingleton<Profile, DirectorProfile>();
            services.AddSingleton<IConfigurationProvider, AutoMapperConfiguration>(p =>
                    new AutoMapperConfiguration(p.GetServices<Profile>()));
            services.AddSingleton<IMapper, Mapper>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IHashService, HashService>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IMovieRepository, MovieRepository>();
            return services;
        }

        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddScoped<IValidator<MovieAddEditDTO>, MovieAddEditDTOValidator>();
            return services;
        }
    }
}
