﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoviesWebApi.Services.Interfaces;
using MoviesWebApi.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesWebApi.WebApi.Controllers
{
    [Route("api/movies")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService movieService;

        public MoviesController(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MovieDTO))]
        public async Task<IActionResult> AddMovieAsync([FromBody] MovieAddEditDTO movieAddDTO)
        {
            var result = await movieService.AddMovieAsync(movieAddDTO);
            return Ok(result);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MovieDTO))]
        public async Task<IActionResult> GetMovieDetailsAsync(int id)
        {
            var result = await movieService.GetMovieDetailsAsync(id);
            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<MovieListItemDTO>))]
        public async Task<IActionResult> GetAllMovieAsync()
        {
            var result = await movieService.GetAllMoviesAsync();
            return Ok(result);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteMovieAsync(int id)
        {
            await movieService.DeleteMovieAsync(id);
            return Ok($"Successfully deleted movie with id = {id}");
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(MovieDTO))]
        public async Task<IActionResult> UpdateMovieAsync(int id, [FromBody] MovieAddEditDTO movieEditDTO)
        {
            if (id != movieEditDTO.Id)
            {
                return BadRequest($"Invalid update Id: {id} != {movieEditDTO.Id}");
            }
            var result = await movieService.UpdateMovieAsync(movieEditDTO);
            return Ok(result);
        }
    }
}
