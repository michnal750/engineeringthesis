﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoviesWebApi.Services.Interfaces;

namespace MoviesWebApi.WebApi.Controllers
{
    [Route("api/hashes")]
    public class HashesController : ControllerBase
    {
        private readonly IHashService hashService;

        public HashesController(IHashService hashService)
        {
            this.hashService = hashService;
        }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
        public IActionResult HashText([FromQuery] string text)
        {
            if(string.IsNullOrEmpty(text))
            {
                return BadRequest("Incorrect parameter");
            }
            var result = hashService.ComputeSha256Hash(text);
            return Ok(result);
        }
    }
}
