﻿using FluentValidation;
using MoviesWebApi.Services.Models;

namespace MoviesWebApi.Services.Validators
{
    public class MovieAddEditDTOValidator : AbstractValidator<MovieAddEditDTO>
    {
        public MovieAddEditDTOValidator()
        {
            RuleFor(p => p.Title)
                .NotEmpty()
                .MaximumLength(20);

            RuleFor(p => p.Rating)
                .InclusiveBetween(1.0f, 10.0f);
        }
    }
}
