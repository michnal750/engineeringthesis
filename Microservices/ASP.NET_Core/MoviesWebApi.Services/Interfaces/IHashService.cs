﻿namespace MoviesWebApi.Services.Interfaces
{
    public interface IHashService
    {
        string ComputeSha256Hash(string text);
    }
}
