﻿using MoviesWebApi.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesWebApi.Services.Interfaces
{
    public interface IMovieService
    {
        Task<MovieDTO> AddMovieAsync(MovieAddEditDTO newMovieDTO);
        Task<MovieDTO> GetMovieDetailsAsync(int id);
        Task<IEnumerable<MovieListItemDTO>> GetAllMoviesAsync();
        Task<MovieDTO> UpdateMovieAsync(MovieAddEditDTO updatedMovieDTO);
        Task DeleteMovieAsync(int id);
    }
}
