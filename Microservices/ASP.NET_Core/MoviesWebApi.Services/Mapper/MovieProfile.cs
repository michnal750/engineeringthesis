﻿using AutoMapper;
using MoviesWebApi.DAL.Entities;
using MoviesWebApi.Services.Models;

namespace MoviesWebApi.Services.Mapper
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>();
            CreateMap<Movie, MovieListItemDTO>();
        }
    }
}
