﻿using AutoMapper;
using MoviesWebApi.DAL.Entities;
using MoviesWebApi.Services.Models;

namespace MoviesWebApi.Services.Mapper
{
    public class DirectorProfile : Profile
    {
        public DirectorProfile()
        {
            CreateMap<Director, DirectorDTO>();
        }
    }
}
