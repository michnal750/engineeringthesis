﻿using AutoMapper;
using MoviesWebApi.DAL.Entities;
using MoviesWebApi.DAL.Interfaces.Repositories;
using MoviesWebApi.Services.Interfaces;
using MoviesWebApi.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesWebApi.Services.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository movieRepository;
        private readonly IMapper mapper;

        public MovieService(IMovieRepository movieRepository, IMapper mapper)
        {
            this.movieRepository = movieRepository;
            this.mapper = mapper;
        }

        public async Task<MovieDTO> AddMovieAsync(MovieAddEditDTO newMovieDTO)
        {
            var entity = new Movie()
            {
                Title = newMovieDTO.Title,
                DirectorId = newMovieDTO.DirectorId,
                ReleaseDate = newMovieDTO.ReleaseDate,
                Rating = newMovieDTO.Rating,
                Plot = newMovieDTO.Plot
            };
            movieRepository.Add(entity);
            await movieRepository.SaveChangesAsync();
            entity = await movieRepository.GetByIdWithDetailsAsync(entity.Id);
            return mapper.Map<MovieDTO>(entity);
        }

        public async Task DeleteMovieAsync(int id)
        {
            var entity = await movieRepository.GetByIdAsync(id);
            movieRepository.Delete(entity);
            await movieRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<MovieListItemDTO>> GetAllMoviesAsync()
        {
            var entites = await movieRepository.GetAllAsync();
            return mapper.Map<IEnumerable<MovieListItemDTO>>(entites);
        }

        public async Task<MovieDTO> GetMovieDetailsAsync(int id)
        {
            var entity = await movieRepository.GetByIdWithDetailsAsync(id);
            return mapper.Map<MovieDTO>(entity);
        }

        public async Task<MovieDTO> UpdateMovieAsync(MovieAddEditDTO updatedMovieDTO)
        {
            var entity = await movieRepository.GetByIdAsync(updatedMovieDTO.Id);

            entity.Title = updatedMovieDTO.Title;
            entity.DirectorId = updatedMovieDTO.DirectorId;
            entity.ReleaseDate = updatedMovieDTO.ReleaseDate;
            entity.Rating = updatedMovieDTO.Rating;
            entity.Plot = updatedMovieDTO.Plot;

            movieRepository.Update(entity);
            await movieRepository.SaveChangesAsync();
            entity = await movieRepository.GetByIdWithDetailsAsync(entity.Id);
            return mapper.Map<MovieDTO>(entity);
        }
    }
}
