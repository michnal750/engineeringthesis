﻿using MoviesWebApi.Services.Interfaces;
using System.Security.Cryptography;
using System.Text;

namespace MoviesWebApi.Services.Services
{
    public class HashService : IHashService
    {
        public string ComputeSha256Hash(string text)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(text));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
