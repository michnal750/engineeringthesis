﻿namespace MoviesWebApi.Services.Models
{
    public class DirectorDTO
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
