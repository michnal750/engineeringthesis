﻿using System;

namespace MoviesWebApi.Services.Models
{
    public class MovieListItemDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public int? DirectorId { get; set; }
    }
}
