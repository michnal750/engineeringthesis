﻿using System;

namespace MoviesWebApi.Services.Models
{
    public class MovieAddEditDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime? ReleaseDate { get; set; }

        public float? Rating { get; set; }

        public string Plot { get; set; }

        public int? DirectorId { get; set; }
    }
}
